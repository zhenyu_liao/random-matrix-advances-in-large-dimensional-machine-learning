\documentclass{article}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\hypersetup{colorlinks=true}
\usepackage{color}

\newcommand{\RED}{\color[rgb]{0.70,0,0}}
\newcommand{\BLUE}{\color[rgb]{0,0,0.69}}

\title{Prospectus \\ ``Random Matrix Advances in Machine Learning''}
\author{Romain Couillet, Zhenyu Liao}
\date{\today}

\begin{document}

\maketitle

\section{Working book title} 
\emph{If you’d like to raise alternatives for comment, please do.}

\medskip 
We are still debating on a most effective title. Mandatory keywords are ``\textbf{random matrices}'' and ``\textbf{machine learning}''. This said, the book has a twofold vision: (i) in the first part, to provide a ``tutorial approach'' to the foundations of random matrix theory for best use in machine learning (so targeting here both engineers/researchers outside the field and undergraduate/graduate students) and (ii) in the second part, to give access to more advanced findings in the domain (therefore targeting more qualified researchers in the field). Hence the possible titles we have in mind are:
\begin{itemize}
    \item {\bf Random Matrix Advances in Machine Learning}: ``advances'' may sound more research-oriented, so closer to (ii) than (i);
    \item {\bf Fundamentals of Random Matrix Theory for Machine Learning} or {\bf Random Matrix Foundations for Machine Learning}: more appropriate to (i) but somehow less to (ii).
\end{itemize}
The key messages to convey in the title are: this book is (a) to the best of knowledge, \emph{the} first book offering a \emph{unified} random matrix approach to machine learning problems and covering most recent advances in the domain and (b) a textbook introducing both technical tools and their applications (to machine learning methods), however, being different from ``classical'' machine learning textbooks by focusing on the ``\textbf{large dimensional}'' scenario beyond the two- or three-dimensional toy examples discussed in some previous machine learning textbooks.

\medskip 
We are open to discussing this aspect and would really appreciate your help and expertise.

\section{Rationale and scope} 
\emph{What's motivating you to write this book? Why is it needed? Why is
it the right time? Why have you taken the approach you have, and chosen to
include/exclude the things you have?}

\medskip 
To answer this question, a brief historical introduction to the (still) young field of random matrices is useful.

\paragraph{Historical point.} Until the early 2000's, random matrix theory (RMT) has been mostly a pure-math endeavor, which was a problem at the time for engineers and applied researchers (myself included [R.\@ Couillet], as a PhD student at that time) since all articles and books on the topic were very hard to access and the subject was not mature enough to be taught in classes. 

Then there has been a first big moment for random matrices when it became clear in 2000-2005 that it could be of huge importance for wireless communication applications: today, it is a mandatory subfield of wireless communications which brought in passing a much clearer understanding and improvement of the new 4G/5G technologies since then. With my colleague Mérouane Debbah, we published a book at CUP  (``\href{https://www.cambridge.org/core/books/random-matrix-methods-for-wireless-communications/6A432BE9C2FE112D0E3F4B7B6568F4F9}{Random Matrix Methods in Wireless Communications}'', in 2011) specifically to fill a hole: there was a huge demand in the wireless communication community to get a grasp on the tool but the math books available back then were simply way out of reach for most applied researchers and engineers. 

A few years later (from 2010 on, say), the fields of application of RMT expanded, particular towards large dimensional statistics. Now, with the resurgence of machine learning (ML) and in particular deep learning (which, as opposed to wireless communication, are more difficult to theorize), RMT is dramatically changing the way machine lean ring problems ought to be tackled; and, possibly more importantly, provides new and more appropriate intuitions that lead to significant improvements in machine learning practices.

\paragraph{Rationale.} To summarize our main motivation for this new book,
\begin{itemize}
    \item[(i)] despite years of research, the theory of RMT is still hard to grasp for people outside the field (except for those from the wireless communication community and those who worked on large dimensional statistics for their own sake before, but that's not a lot of people) and so we want a better, more digestible version, with more intuitions, and containing a lot of (reproducible) figures and application-oriented exercises;
    \item[(ii)] at the same time, the number of scientific articles on the subject is literally exploding in prestigious ML conferences such as ICML/NeurIPS so that RMT affirms itself as a key ingredient for the future understanding and improvement of ML; an accessible book on the topic for the (rapidly growing) machine learning community becomes a necessity; we also want the book to be written ``more like an ML book'' (that is, with codes, extensive simulations on both synthetic and real data, a lot of insights) rather than like a math book: to the best of our knowledge, no such book exists yet;
    \item[(iii)] as a consequence of (i) and (ii), many master-level classes are now being created around the topic in Europe, the US and Asia (as far as we know). I have myself [R.\@ Couillet] been teaching two classes in random matrices for wireless communication and for machine learning in prestigious master programs at University Paris Saclay (see e.g., the MVA master \href{http://math.ens-paris-saclay.fr/version-francaise/formations/master-mva/contenus-/theorie-des-matrices-aleatoires-et-apprentissage-214242.kjsp?RH=1242430202531}{class}), respectively since 2010 and 2013. A similar course (\href{https://www.stat.berkeley.edu/~songmei/Teaching/STAT260_Spring2021/index.htm}{STAT260: Mean Field Asymptotics in Statistical Learning}) is provided at University of California, Berkeley, where Z.\@ Liao serves as a guest lecturer. Colleagues (among whom \href{https://facultyprofiles.ust.hk/profiles.php?profile=matthew-robert-mckay-eemrmckay}{Matthew McKay}) at Hong-Kong University of Science and Technologies are also providing similar programs in applied random matrices for statistics and signal processing. Our new book can, beyond providing advanced state-of-the-art research results, also be used in these training programs.
\end{itemize}

\paragraph{What the book includes/excludes.} In a nutshell, the main features of the book are:
\begin{itemize}
    \item it is divided in two main parts: an introduction to the basics of random matrices (just the necessary tools needed for addressing machine learning problems, which distinguishes it from pure math books) and a second longer part on the many applications to machine learning (from SVMs through semi-supervised learning, unsupervised spectral clustering, graph methods, to neural networks).
    \item the first part contains short exercises for the reader to get familiar with the technical notions; it also contains many graphs and figures which the readers can reproduce from the attached codes (with links systematically provided below each figure in the digital version of the book); these codes are available both in \texttt{Matlab} and \texttt{Python}, for both math- and ML-oriented readers.
    \item the second part is divided into six chapters tackling (in effect revisiting) key methods in machine learning from a random matrix perspective. In each chapter, we start by reminding the readers of the ``classical'' machine learning methods, discuss the different intuition offered by RMT, followed by a systematic random matrix analysis of the performance and the possible improvements, and illustrate these results on synthetic as well as real-world data. An important feature of these chapters is that, at the end of each chapter, ``guided exercises'' which we called ``practical lecture material'' (maybe to be renamed) are provided to include additional results, not covered in the core of the chapter, in a ``lab session approach'' with step-by-step instructions, with codes also made available. These longer exercises may serve as independent lab sessions or exams.
    \item as a book dedicated to understanding and improving mostly non-linear machine learning algorithms, a salient feature of the monograph is a strong dedication to \emph{non-linear as well as structured random matrix models}; this is quite uncommon in math-oriented textbooks on RMT which rather focus on the base (linear, with little structure) models.  Concretely, particular emphasis is set on kernel random matrix models, non-linear (random) neural network models, and mixture-data models with differing statistical means and covariance structures as reliable ``proxies'' to real-world machine learning.
\end{itemize}

So the proposed book differs from most existing random matrix books that are either pure math books with hardly any application, or restricted to toy models (two- or three-dimensional synthetic data) that are far from modern ML practice. This naturally means that our book excludes some aspects of these more math-oriented monographs:
\begin{itemize}
    \item while the first theoretical part of the book is mathematically thorough (with everything proved carefully), the second application part rather provides sketches of the proofs when necessary, with the main purpose to go beyond the main proof ingredients and to focus on the practical implications (for interested readers, external sources and references will be provided);
    \item even in the first part of the book, we provide insightful intuitions, figures and remarks (rather than pure math proofs) for an easier grasp of the main technical results;
    \item some mathematical aspects of RMT have been voluntarily left aside as inessential for machine learning applications; this is particularly the case for (i) the question of local spectra (microscopic fluctuations of eigenvalues), (ii) the newer subfield of non-Hermitian random matrix theory (a very important field in mathematics but which, to this day, has found practically no machine learning application), or (iii) the relation between the theory of sparse random graphs and random matrix theory, that does find applications in machine learning but requires very different toots from those presented in this monograph.
\end{itemize}

\section{Readership}
\emph{What is your intended core readership? What benefits will your book
bring to this readership? What are the essential prerequisites that you are assuming of readers? Is there further knowledge that is helpful but not essential?}

\emph{If you expect your book to be adopted as a text for courses, in which department(s) is the course taught, at what level, and what are the most common course names? If you have existing courses/universities in mind, please supply this information.}


\paragraph{Rationale.} The book discusses both theoretical basics of random matrix theory for machine learning and state-of-the-art advances in some specific applications (which unfold in fact quite naturally from the basics). As such, our main readers should fall in several categories:
\begin{itemize}
    \item machine learning experts that are not familiar with large dimensional statistics, and specifically with random matrix theory: the introduction of the concepts and applications in an ML-oriented discourse, with codes available, allow them to quickly ramp up to the domain;
    \item educational programs targeting master or PhD-level students for which this book can be used as support material: exercises complete the first technical sections as in traditional math classes, figures and codes are provided for the students to try on their own, practical lab sessions can be organized either by digging into the long list of codes in the technical part or, more appropriately, by directly using the ``practical course material'' provided at the end of each applied chapter;
    \item random matrix experts that may find this book an interesting opening for expanding their technical work to the important applied fields of ML and AI. The big push of AI today attracts researchers from all horizons, and the second part of the book provides illustrating application examples for math experts from the domain of large dimensional statistics.
\end{itemize}

\paragraph{University programs.} Master and PhD programs are mushrooming in the fields of ML and AI today. At University Paris-Saclay where I (R.\@ Couillet) teach, the prestigious \href{http://math.ens-paris-saclay.fr/version-francaise/formations/master-mva/}{MVA master program} (math-vision-learning), which formerly attracted up to $20$ students, is now filled with no less than $150$ students per year. In my own class of ``\href{http://math.ens-paris-saclay.fr/version-francaise/formations/master-mva/contenus-/theorie-des-matrices-aleatoires-et-apprentissage-214242.kjsp?RH=1242430202531}{Introduction to random matrices for machine learning}'', I have up to 30 students annually (and growing). This observation holds worldwide with an increasing attraction of ML/AI classes for students coming from diverse fundamental and engineering fields, in particular, probability, statistics, signal processing, wireless communication, and even theoretical physics. 

The material of the book is well adapted to these students. The prerequisites are basic undergraduate levels in probability theory (distributions, convergence), linear algebra (symmetric matrix, eigen-decomposition), and complex analysis (contour integrals, residues), along with elementary notions in statistics (tests, detection and estimation) and machine learning (basics in classification, regression). The book in particular avoids digging
too deeply into the complicated mathematical concepts of these fields (martingale theory tools, operators in Hilbert spaces beyond $\mathbb R^n$, advanced analyticity notions, reproducting kernel theory, etc., are not at all discussed).

\section{Competing/related books}
\emph{What will your book offer that isn't available elsewhere? How does your book relate to other books on the topic currently in use? Please also mention books that are not directly competitive but whose readers you wish to reach.}

\medskip
To the best of our knowledge, this is the first book which bridges random matrix theory and machine learning. We wish to reach readers having a general interested in (statistical) machine learning, for example readers of the following books:
\begin{itemize}
    \item ``The Elements of Statistical Learning'', by Jerome H. Friedman, Robert Tibshirani, and Trevor Hastie;
    \item ``Pattern Recognition and Machine Learning'' by Christopher Bishop;
    \item ``An Introduction to Statistical Learning: With Applications in R'' by Gareth M. James, Daniela Witten, Trevor Hastie, and Robert Tibshirani;
    \item ``Machine Learning: A Probabilistic Perspective'' by Kevin P. Murphy.
\end{itemize}
Since machine learning (for large dimensional data particularly) is of rapidly growing research interest today, we would also like to reach readers from other fields (from electrical engineering to pure math) with some random matrix theory background, such as the readers of 
\begin{itemize}
    \item ``Random Matrix Methods for Wireless Communications'', by Romain Couillet and Mérouane Debbah;
    \item ``Smart Grid Using Big Data Analytics: A Random Matrix Theory Approach'' by Robert Qiu and Paul Antonik;
    \item ``An Introduction to Random Matrices'' by Greg W. Anderson, Alice Guionnet and Ofer Zeitouni;
    \item ``Spectral Analysis of Large Dimensional Random Matrices'' by Zhidong Bai and Jack Silverstein;
    \item ``Eigenvalue Distribution of Large Random Matrices'' by Leonid Pastur and Mariya Shcherbina;
    \item ``High-Dimensional Probability: An Introduction with Applications in Data Science'' by Roman Vershynin;
\end{itemize}
that may be interested in diving into machine learning problems from a random matrix perspective.

Of these books, the closest in spirit to our present monograph is our former book ``Random Matrix Methods for Wireless Communications'' published at CUP in 2011, and ``High-Dimensional Probability: An Introduction with Applications in Data Science'' by Vershynin. 

The differences between our previous book (already 10 years of age) and the proposed manuscript are numerous: for once, the problems raised by wireless communications (linear statistics of the \emph{eigenvalues} of \emph{linear} random matrix models) are quite different from those addressed in machine learning (mostly addressing \emph{eigenvectors} and advanced statistics of \emph{non-linear} random matrix models); also, the technical approach (focused on the Stieltjes transform of matrices with independent entries in 2011) has now been steered (for reasons of modernity, convenience, and strength of the results) towards a more modern and powerful \emph{resolvent-based} approach with \emph{Gaussian integration by part} technique as an important technical tool. Of course, technicalities aside, the core of the proposed book is centered on machine learning applications which have no direct connection to wireless communication problems.

As for Vershynin's book, closest to our manuscript ``in spirit'' as it addresses data science, it takes a radically different angle, towards what he refers to as ``non-asymptotic random matrix theory'' (in a way vaguely connected to our Section~2.7 on concentration of measure theory). Unlike our book which aims to evaluate the \emph{exact performance} of \emph{non-linear} machine learning methods, Vershynin's book rather focuses on linear problems and exploits concentration inequalities to provide non-asymptotic control (rather than in general precisely estimate) of the performance of machine learning methods. The meager overlap of the concentration of measure theory toolbox which we introduce in a different manner does create a bridge between both books but which otherwise remain fairly independent.

\section{Detailed table of contents} 
\emph{Describe the contents of each chapter, e.g. section headings or short summaries. Will there be exercises? Paper‐and‐pencil? Data‐based? }

\medskip
We already have an almost complete version of the book. The detailed table of contents, duplicated below, is self-explanatory:

\begin{itemize}
    \item Chapter 1 Introduction
    \begin{itemize}
        \item Section 1.1 Motivation: the pitfalls of large dimensional statistics
        \item Section 1.2 Random matrix theory as an answer
        \item Section 1.3 Outline and online toolbox
    \end{itemize}
    \item Chapter 2 Basics of Random Matrix Theory
    \begin{itemize}
        \item Section 2.1 Fundamental objects
        \item Section 2.2 Foundational random matrix results
        \item Section 2.3 Advanced spectrum considerations for sample covariances
        \item Section 2.4 Preliminaries on statistical inference
        \item Section 2.5 Spiked models
        \item Section 2.6 Information-plus-noise, deformed Wigner, and other models
        \item Section 2.7: Beyond vectors of independent entries: concentration of measure in RMT 
        \item Section 2.8 Concluding remarks
        \item Section 2.9 Exercises (paper‐and‐pencil exercises to familiarize the readers with the technical tools)
    \end{itemize}
    \item Chapter 3 Statistical Inference in Linear Models
    \begin{itemize}
        \item Section 3.1 Detection and estimation in information-plus-noise models
        \item Section 3.2 Covariance matrix distance estimation
        \item Section 3.3 M-estimators of scatter
        \item Section 3.4 Concluding remarks
        \item Section 3.5 Practical course material (``The Wasserstein distance estimation'' and ``Robust portfolio optimization via Tyler estimator'')
    \end{itemize}
    \item Chapter 4 Kernel Methods
    \begin{itemize}
        \item Section 4.1 Basic setting
        \item Section 4.2 Distance and inner-product random kernel matrices
        \item Section 4.3 The $\alpha$-$\beta$ random kernel model
        \item Section 4.4 Properly scaling kernel model
        \item Section 4.5 Implications to kernel methods (unsupervised kernel spectral clustering, semi-supervised graph-based kernel learning, and supervised kernel ridge regression)
        \item Section 4.6 Concluding remarks
        \item Section 4.7 Practical course material (``Complexity-performance trade-off in spectral clustering with sparse kernel'' and ``Towards transfer learning with kernel regression'')
    \end{itemize}
    \item Chapter 5 Large Neural Networks
    \begin{itemize}
        \item Section 5.1 Random neural networks
        \item Section 5.2 Gradient descent dynamics in learning linear neural nets
        \item Section 5.3 Recurrent neural nets: echo state networks
        \item Section 5.4 Concluding remarks
        \item Section 5.5 Practical course material (``Effective kernel of large dimensional random Fourier features'')
    \end{itemize}
    \item Chapter 6 Optimization-based Methods with Non-explicit Solutions
    \begin{itemize}
        \item Section 6.1 Generalized linear classifier
        \item Section 6.2 Large dimensional support vector machines 
        \item Section 6.3 Concluding remarks
        \item Section 6.4 Practical course material (``Phase retrieval'')
    \end{itemize}
    \item Chapter 7 Community Detection on Graphs
    \begin{itemize}
        \item Section 7.1 Community detection in dense graphs
        \item Section 7.2 From dense to sparse graphs: a different approach
        \item Section 7.3 Concluding remarks
        \item Section 7.4 Practical course material (``Gaussian fluctuations of the SBM eigenvectors'')
    \end{itemize}
    \item Chapter 8 Discussions on Universality and Practical Applications
    \begin{itemize}
        \item Section 8.1 From Gaussian mixtures to concentrated random vectors and GAN images
        \item Section 8.2 Wide-sense university in large dimensional machine learning 
        \item Section 8.3 Discussions and conclusions
    \end{itemize}
\end{itemize}

\smallskip
Both paper-and-pencil (in Section~2.9) and data-based (at the end of each chapter) exercises will be provided, with solutions and codes given at the end of the book. 


\section{Special features}
\emph{For example, will any of your figures require color in order to be
understood? If so, please estimate how many will need to be in color.}

\medskip

The book contains a total of $84$ figures, some of them not necessarily requiring color, although most of them do. Being a book on the topic of machine learning, with many applications to image classification, the book contains images which are best visualized in color. In this sense, this is quite different from math/stats-oriented books which do no provide many figures and, if and, are often simple graphs that can live without color.

This is a topic which we would best discuss with CUP editors (along with the optimal way to include our clickable ``Links to Code'' in the printed version of the book).

\section{Description of any ancillary material}
\emph{Will you provide computer code (what language/package), data sets, solutions to exercises, slides for lecturers, etc.?}

\medskip 
Computer (\texttt{Matlab} and \texttt{Python}) codes to reproduce the figures in the book will be available at \url{https://github.com/Zhenyu-LIAO/RMT4ML}, all used data sets are public, solutions to exercises are given at the end of the book (with codes available at \url{https://github.com/Zhenyu-LIAO/RMT4ML}).

We wish to emphasize the fact that our codes are provided along with a HTML output with comments and generated figures ``in place'' (in a Jupyter Notebook export fashion) for accessibility and ease of use.

\section{Proposed length of the book and its proposed completion date}
\emph{These needn't be set in stone, but please make them as realistic as you can.}

\medskip 
The book will be about $\mathbf{400}$ \textbf{page} long, under the ``\texttt{book}'' document-class of LateX (likely much longer under the CUP format). It is expected to be completed by, say, the end of \textbf{March 2021}.

\section{Brief credentials of the author(s)}
\emph{(N.B. a full curriculum vitae is not required).}

\paragraph{\href{https://romaincouillet.hebfree.org/index.html}{Romain COUILLET}} received his MSc in Mobile Communications at the Eurecom Institute and his MSc in Communication Systems in Telecom ParisTech, France in 2007. From 2007 to 2010, he worked with ST-Ericsson as an Algorithm Development Engineer on the Long Term Evolution Advanced project, where he prepared his Ph.D. with Supelec, France, which he graduated in November 2010. He is currently a Full Professor in the L2S laboratory at \href{https://www.centralesupelec.fr/en}{CentraleSup\'elec}, \href{https://www.universite-paris-saclay.fr/en}{University of Paris-Saclay}, France, and the holder of the \href{https://romaincouillet.hebfree.org/gstats.html}{MIAI LargeDATA DataScience Chair} at \href{https://www.univ-grenoble-alpes.fr/english/}{University Grenoble-Alpes}, France. His research topics are in machine learning, signal processing, and in theoretical and applied random matrix theory. He is a co-author of the book ``\href{https://www.cambridge.org/core/books/random-matrix-methods-for-wireless-communications/6A432BE9C2FE112D0E3F4B7B6568F4F9}{Random matrix methods for wireless communications}'', Cambridge University Press, 2011. He is the recipient of the 2013 CNRS Bronze Medal in the section ``science of information and its interactions'', of the 2013 IEEE ComSoc Outstanding Young Researcher Award (EMEA Region), of the 2011 EEA/GdR ISIS/GRETSI best Ph.D. thesis award, and of the Valuetools 2008 best student paper award. He gave nine (9) tutorials on random matrix theory applied to signal processing and machine learning in international IEEE conferences (up to $100$ attendees). He also own 4 patents in signal processing and wireless communication applications.

Key figures (according to Google Scholar as of December 2020): 
\begin{itemize}
    \item Citations: 3800+
    \item h-index: 28
    \item i10-index: 67
    \item Citations of previous CUP book: 805
    \item Five best article references: 805, 571, 159, 124, 93.
\end{itemize}

\paragraph{\href{https://zhenyu-liao.github.io/}{Zhenyu LIAO}} is a Postdoctoral Researcher at \href{https://www.berkeley.edu/}{University of California, Berkeley}, Department of Statistics (hosted by Prof. \href{https://www.stat.berkeley.edu/~mmahoney/}{Michael Mahoney}). Zhenyu Liao received his Ph.D.\@ from \href{https://www.centralesupelec.fr/en}{CentraleSup\'elec}, \href{https://www.universite-paris-saclay.fr/en}{University of Paris-Saclay} in 2019, where he worked under the supervision of Prof.\@ \href{https://romaincouillet.hebfree.org/index.html}{Romain Couillet} and Prof.\@ \href{https://scholar.google.com/citations?user=L6Fy4cgAAAAJ&hl=en}{Yacine Chitour}. He received a B.Sc.\@ degree in Optical \& Electronic Information from Huazhong University of Science \& Technology, China, and an M.Sc.\@ degree in Signal and Image Processing from \href{https://www.universite-paris-saclay.fr/en}{University of Paris-Saclay}, France, in 2016. His research interests are broadly in machine learning, signal processing, random matrix theory, and high-dimensional statistics. He is the recipient of the ED-STIC Ph.D.\@ Student Award and the Sup\'elec Foundation Ph.D. Fellowship of \href{https://www.universite-paris-saclay.fr/en}{University of Paris-Saclay}, France.

Key figures (according to Google Scholar as of December 2020): 
\begin{itemize}
    \item Citations: 195
    \item h-index: 7
    \item i10-index: 6
    \item Five best article references: 66, 36, 23, 16, 16.
\end{itemize}

\end{document}
