\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}%
\contentsline {section}{\numberline {1.1}Motivation: the pitfalls of large dimensional statistics}{7}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}The big data era: when $n$ is no longer much larger than $p$ }{7}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Sample covariance matrices in the large $n,p$ regime}{8}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Kernel matrices of large dimensional data}{11}{subsection.1.1.3}%
\contentsline {subsubsection}{The non-trivial classification regime}{12}{section*.4}%
\contentsline {subsubsection}{Asymptotic loss of pairwise distance discrimination}{14}{section*.5}%
\contentsline {subsubsection}{Explaining kernel methods with random matrix theory}{15}{section*.7}%
\contentsline {subsubsection}{Do real data follow small or large dimensional intuitions?}{16}{section*.8}%
\contentsline {subsection}{\numberline {1.1.4}Summarizing}{17}{subsection.1.1.4}%
\contentsline {section}{\numberline {1.2}Random matrix theory as an answer}{19}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Which theory and why?}{19}{subsection.1.2.1}%
\contentsline {subsubsection}{A point of history}{19}{section*.11}%
\contentsline {subsubsection}{Resolvents, Gaussian tools, and concentration of measure theory}{21}{section*.12}%
\contentsline {subsection}{\numberline {1.2.2}The double asymptotics: turning the curse of dimensionality into a dimensionality blessing}{24}{subsection.1.2.2}%
\contentsline {subsubsection}{Why random matrix theory to study the large $n,p$ regime?}{24}{section*.13}%
\contentsline {subsubsection}{The case of machine learning}{25}{section*.14}%
\contentsline {subsection}{\numberline {1.2.3}Analyze, understand, and improve large dimensional machine learning methods}{27}{subsection.1.2.3}%
\contentsline {subsubsection}{From low to large dimensional intuitions}{28}{section*.15}%
\contentsline {subsubsection}{Core random matrices in machine learning algorithms}{29}{section*.17}%
\contentsline {subsubsection}{Performance analysis: spectral properties and functionals}{31}{section*.18}%
\contentsline {subsubsection}{Directions of improvement and new ideas}{32}{section*.19}%
\contentsline {subsection}{\numberline {1.2.4}Exploiting universality: from large dimensional Gaussian vectors to real data}{33}{subsection.1.2.4}%
\contentsline {subsubsection}{Theory versus practice}{33}{section*.20}%
\contentsline {subsubsection}{Concentrated random vectors and real data modeling}{35}{section*.21}%
\contentsline {section}{\numberline {1.3}Outline and online toolbox}{39}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Organization of the manuscript}{39}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Online codes}{42}{subsection.1.3.2}%
\contentsline {chapter}{\numberline {2}Random Matrix Theory}{43}{chapter.2}%
\contentsline {section}{\numberline {2.1}Fundamental objects}{44}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}The resolvent}{44}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Spectral measure and Stieltjes transform}{44}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Cauchy's integral, linear eigenvalue functionals, and eigenspaces}{47}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}Deterministic and random equivalents}{49}{subsection.2.1.4}%
\contentsline {section}{\numberline {2.2}Foundational random matrix results}{51}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Key lemmas and identities}{52}{subsection.2.2.1}%
\contentsline {subsubsection}{Resolvent identities}{52}{section*.25}%
\contentsline {subsubsection}{Perturbation identities}{53}{section*.26}%
\contentsline {subsubsection}{Probability identities}{57}{section*.28}%
\contentsline {subsection}{\numberline {2.2.2}The Mar{\u c}enko-Pastur and semicircle laws}{59}{subsection.2.2.2}%
\contentsline {subsubsection}{The Mar{\u c}enko-Pastur law}{59}{section*.29}%
\contentsline {paragraph}{Intuitive idea.}{61}{section*.31}%
\contentsline {paragraph}{Detailed proof of Theorem\nobreakspace {}\ref {theo:MP-law}.}{63}{section*.32}%
\contentsline {paragraph}{\it Convergence in mean.}{63}{section*.33}%
\contentsline {paragraph}{\it Concentration and almost sure convergence.}{68}{section*.34}%
\contentsline {subsubsection}{The ``Gaussian method'' alternative}{70}{section*.35}%
\contentsline {paragraph}{\it Convergence in mean by Stein's lemma.}{70}{section*.36}%
\contentsline {paragraph}{\it Concentration and almost sure convergence by Nash--Poicar\'e inequality.}{72}{section*.37}%
\contentsline {paragraph}{\it Interpolation trick to non-Gaussian ${\mathbf {X}}$.}{73}{section*.38}%
\contentsline {subsubsection}{Wigner semicircle law}{74}{section*.39}%
\contentsline {paragraph}{\it Bai-Silverstein heuristic.}{76}{section*.41}%
\contentsline {paragraph}{\it Gaussian method heuristic.}{77}{section*.42}%
\contentsline {subsection}{\numberline {2.2.3}Large dimensional sample covariance matrices and generalized semicircles}{77}{subsection.2.2.3}%
\contentsline {subsubsection}{Large sample covariance matrix model and its generalizations}{78}{section*.43}%
\contentsline {subsubsection}{Generalized semicircle law with a variance profile}{89}{section*.45}%
\contentsline {section}{\numberline {2.3}Advanced spectrum considerations for sample covariances}{91}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Limiting spectrum}{91}{subsection.2.3.1}%
\contentsline {subsubsection}{Density and support of $\mu $ (and $\tilde \mu $)}{92}{section*.46}%
\contentsline {subsubsection}{Variable change: relating $\supp (\nu )$ and $\supp (\mu )$.}{95}{section*.48}%
\contentsline {subsection}{\numberline {2.3.2}``No eigenvalue outside the support''}{95}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Preliminaries on statistical inference}{98}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Linear eigenvalue statistics}{99}{subsection.2.4.1}%
\contentsline {subsubsection}{Relating population and sample Stieltjes transforms}{99}{section*.50}%
\contentsline {subsubsection}{Eigen-inference}{100}{section*.51}%
\contentsline {subsubsection}{Application example: estimating population eigenvalues of large multiplicity}{102}{section*.52}%
\contentsline {paragraph}{Fully separable case.}{103}{section*.53}%
\contentsline {paragraph}{Non-separable case.}{106}{section*.56}%
\contentsline {subsection}{\numberline {2.4.2}Eigenvector projections and subspace methods}{107}{subsection.2.4.2}%
\contentsline {subsubsection}{Estimates of functionals of ${\mathbf {X}}$}{108}{section*.57}%
\contentsline {paragraph}{Example: Eigenspace correlation.}{109}{section*.58}%
\contentsline {subsubsection}{Eigenvector inference and subspace methods}{113}{section*.61}%
\contentsline {section}{\numberline {2.5}Spiked models}{113}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Isolated eigenvalues}{114}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Isolated eigenvectors}{119}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Limiting fluctuations}{122}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}Further discussions and other spiked models}{125}{subsection.2.5.4}%
\contentsline {section}{\numberline {2.6}Information-plus-noise, deformed Wigner, and other models}{127}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}Why focus on the sample covariance matrix model?}{127}{subsection.2.6.1}%
\contentsline {subsection}{\numberline {2.6.2}Other models}{129}{subsection.2.6.2}%
\contentsline {subsubsection}{Advanced sample covariance matrices}{129}{section*.65}%
\contentsline {subsubsection}{Advanced Wigner matrices}{131}{section*.66}%
\contentsline {subsubsection}{(Real) Haar random matrices}{131}{section*.67}%
\contentsline {subsubsection}{The free probability approach}{134}{section*.68}%
\contentsline {subsubsection}{Full circle law, $\beta $-ensembles, sparse random matrices, etc.}{138}{section*.69}%
\contentsline {subsection}{\numberline {2.6.3}Other statistics}{141}{subsection.2.6.3}%
\contentsline {section}{\numberline {2.7}Beyond vectors of independent entries: concentration of measure in RMT}{143}{section.2.7}%
\contentsline {subsection}{\numberline {2.7.1}Limitations of the i.i.d.\spacefactor \@m {} assumption}{143}{subsection.2.7.1}%
\contentsline {subsection}{\numberline {2.7.2}Concentrated random vectors as the answer}{144}{subsection.2.7.2}%
\contentsline {subsection}{\numberline {2.7.3}Elements of concentration of measure for random matrices}{148}{subsection.2.7.3}%
\contentsline {subsubsection}{Concentration of random variables}{148}{section*.70}%
\contentsline {subsubsection}{Concentration of random vectors}{150}{section*.71}%
\contentsline {paragraph}{Linear concentration}{150}{section*.72}%
\contentsline {paragraph}{Lipschitz concentration}{151}{section*.73}%
\contentsline {paragraph}{Convex (Lipschitz) concentration.}{153}{section*.74}%
\contentsline {paragraph}{Convex concentration transversally to a group action.}{153}{section*.75}%
\contentsline {subsection}{\numberline {2.7.4}A concentration inequality version of Theorem\nobreakspace {}\ref {theo:baisilverstein95}}{154}{subsection.2.7.4}%
\contentsline {subsubsection}{Trace lemma}{154}{section*.76}%
\contentsline {subsubsection}{Concentration of the Stieltjes transform}{155}{section*.77}%
\contentsline {subsubsection}{Concentration of the resolvent ${\mathbf {Q}}$ and its deterministic equivalents}{155}{section*.78}%
\contentsline {subsubsection}{Main result}{157}{section*.79}%
\contentsline {section}{\numberline {2.8}Concluding remarks}{160}{section.2.8}%
\contentsline {section}{\numberline {2.9}Exercises}{162}{section.2.9}%
\contentsline {subsection}{\numberline {2.9.1}Properties of the Stieltjes transform}{162}{subsection.2.9.1}%
\contentsline {subsection}{\numberline {2.9.2}On limiting laws}{163}{subsection.2.9.2}%
\contentsline {subsection}{\numberline {2.9.3}On eigen-inference}{164}{subsection.2.9.3}%
\contentsline {subsection}{\numberline {2.9.4}Spiked models}{165}{subsection.2.9.4}%
\contentsline {subsection}{\numberline {2.9.5}Deterministic equivalent}{166}{subsection.2.9.5}%
\contentsline {subsection}{\numberline {2.9.6}Concentration of measure}{167}{subsection.2.9.6}%
\contentsline {subsection}{\numberline {2.9.7}Beyond matrices}{167}{subsection.2.9.7}%
\contentsline {chapter}{\numberline {3}Statistical Inference in Linear Models}{169}{chapter.3}%
\contentsline {section}{\numberline {3.1}Detection and estimation in information-plus-noise models}{171}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}GLRT asymptotics}{171}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Linear and Quadratic Discriminant Analysis}{174}{subsection.3.1.2}%
\contentsline {subsubsection}{Linear discriminant analysis}{175}{section*.81}%
\contentsline {subsubsection}{Quadratic discriminant analysis}{181}{section*.83}%
\contentsline {subsection}{\numberline {3.1.3}Subspace methods: the G-MUSIC algorithm}{183}{subsection.3.1.3}%
\contentsline {subsubsection}{The MUSIC algorithm}{183}{section*.84}%
\contentsline {subsubsection}{Spiked G-MUSIC}{185}{section*.85}%
\contentsline {section}{\numberline {3.2}Covariance matrix distance estimation}{187}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Distances and divergences between Gaussian laws}{187}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}The random matrix framework}{189}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Closed-form expressions}{192}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}The Wasserstein and Frobenius distances}{197}{subsection.3.2.4}%
\contentsline {subsection}{\numberline {3.2.5}Application to covariance-based spectral clustering}{199}{subsection.3.2.5}%
\contentsline {section}{\numberline {3.3}M-estimators of scatter}{200}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Reminder on robust statistics}{200}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}The M-estimator of scatter}{201}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}The random matrix framework}{202}{subsection.3.3.3}%
\contentsline {subsubsection}{A few words on the rigorous proof}{208}{section*.95}%
\contentsline {subsection}{\numberline {3.3.4}Extensions}{210}{subsection.3.3.4}%
\contentsline {paragraph}{Tyler's rotational invariant estimator.}{210}{section*.96}%
\contentsline {paragraph}{The $p>n$ scenario.}{211}{section*.97}%
\contentsline {paragraph}{Robustness to arbitrary outliers.}{212}{section*.98}%
\contentsline {paragraph}{Second-order statistics.}{213}{section*.99}%
\contentsline {section}{\numberline {3.4}Concluding remarks}{215}{section.3.4}%
\contentsline {section}{\numberline {3.5}Practical course material}{216}{section.3.5}%
\contentsline {chapter}{\numberline {4}Kernel Methods}{225}{chapter.4}%
\contentsline {section}{\numberline {4.1}Basic setting}{226}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}The non-trivial growth rates}{227}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Statistical data model}{228}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Distance and inner-product random kernel matrices}{229}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Main intuitions}{230}{subsection.4.2.1}%
\contentsline {paragraph}{Euclidean random matrices with equal covariances.}{230}{section*.102}%
\contentsline {paragraph}{Including covariance structures.}{232}{section*.103}%
\contentsline {paragraph}{Nonlinear kernel models.}{234}{section*.104}%
\contentsline {subsection}{\numberline {4.2.2}Main results: distance random kernel matrices}{236}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Motivation: $\alpha $-$\beta $ random kernel matrices}{241}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Main results: $\alpha $-$\beta $ kernel random matrices}{243}{subsection.4.2.4}%
\contentsline {section}{\numberline {4.3}Properly scaling kernel model}{247}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Motivation}{247}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Setting}{249}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Limiting spectrum of the null model}{251}{subsection.4.3.3}%
\contentsline {subsection}{\numberline {4.3.4}Main results: properly scaling random kernel matrices}{258}{subsection.4.3.4}%
\contentsline {paragraph}{Universality and optimality.}{260}{section*.111}%
\contentsline {paragraph}{Computationally efficient kernels.}{261}{section*.112}%
\contentsline {section}{\numberline {4.4}Implications to kernel methods}{262}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Application to kernel spectral clustering}{263}{subsection.4.4.1}%
\contentsline {subsubsection}{The case of standard distance-kernels}{264}{section*.114}%
\contentsline {paragraph}{Implementation on real data.}{267}{section*.117}%
\contentsline {subsubsection}{The case of ``$\alpha $-$\beta $'' and properly scaling kernels}{270}{section*.120}%
\contentsline {subsection}{\numberline {4.4.2}Application to semi-supervised kernel learning}{273}{subsection.4.4.2}%
\contentsline {subsubsection}{Semi-supervised graph Laplacian and random walk approaches}{274}{section*.124}%
\contentsline {subsubsection}{Large dimensional performance analysis}{276}{section*.125}%
\contentsline {paragraph}{First intuitions.}{277}{section*.126}%
\contentsline {paragraph}{Derivations and main results.}{278}{section*.129}%
\contentsline {subsubsection}{Improving semi-supervised learning}{282}{section*.131}%
\contentsline {paragraph}{Main intuition.}{282}{section*.132}%
\contentsline {paragraph}{Adapted optimization framework.}{283}{section*.133}%
\contentsline {paragraph}{Performance analysis.}{283}{section*.134}%
\contentsline {paragraph}{Application to real-world datasets.}{284}{section*.137}%
\contentsline {subsection}{\numberline {4.4.3}Application to kernel ridge regression}{287}{subsection.4.4.3}%
\contentsline {subsubsection}{Large dimensional performance analysis and its implications}{288}{section*.139}%
\contentsline {subsubsection}{Application to real-world data}{291}{section*.141}%
\contentsline {subsection}{\numberline {4.4.4}Summary of Section\nobreakspace {}\ref {sec:implications_to_kernel_methods}}{291}{subsection.4.4.4}%
\contentsline {section}{\numberline {4.5}Concluding remarks}{294}{section.4.5}%
\contentsline {section}{\numberline {4.6}Practical course material}{295}{section.4.6}%
\contentsline {chapter}{\numberline {5}Large Neural Networks}{299}{chapter.5}%
\contentsline {section}{\numberline {5.1}Random neural networks}{299}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Regression with random neural networks}{300}{subsection.5.1.1}%
\contentsline {subsubsection}{Intuition and main results}{302}{section*.146}%
\contentsline {subsubsection}{Consequences for learning with large neural networks}{305}{section*.147}%
\contentsline {paragraph}{Model complexity and the double descent phenomenon.}{308}{section*.151}%
\contentsline {subsection}{\numberline {5.1.2}Delving deeper into limiting kernels}{311}{subsection.5.1.2}%
\contentsline {section}{\numberline {5.2}Gradient descent dynamics in learning linear neural nets}{318}{section.5.2}%
\contentsline {subsubsection}{Main results}{320}{section*.160}%
\contentsline {subsubsection}{Practical implications}{322}{section*.162}%
\contentsline {section}{\numberline {5.3}Recurrent neural nets: echo state networks}{325}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}Preliminaries and echo state networks}{325}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Results on ESN asymptotics}{327}{subsection.5.3.2}%
\contentsline {section}{\numberline {5.4}Concluding remarks}{333}{section.5.4}%
\contentsline {section}{\numberline {5.5}Practical course material}{335}{section.5.5}%
\contentsline {chapter}{\numberline {6}Large Dimensional Convex Optimization}{339}{chapter.6}%
\contentsline {section}{\numberline {6.1}Generalized linear classifier}{340}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Basic setting}{340}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Intuitions and main results}{341}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Practical consequences and further discussions}{347}{subsection.6.1.3}%
\contentsline {subsubsection}{The existence and uniqueness of the empirical risk minimizer}{348}{section*.171}%
\contentsline {subsubsection}{Implications to large dimensional empirical risk minimization}{349}{section*.172}%
\contentsline {paragraph}{Debiasing the estimator in large dimensions.}{349}{section*.173}%
\contentsline {paragraph}{Optimal loss for classification.}{351}{section*.175}%
\contentsline {section}{\numberline {6.2}Large dimensional support vector machines}{353}{section.6.2}%
\contentsline {section}{\numberline {6.3}Concluding remarks}{358}{section.6.3}%
\contentsline {section}{\numberline {6.4}Practical course material}{361}{section.6.4}%
\contentsline {chapter}{\numberline {7}Community Detection on Graphs}{365}{chapter.7}%
\contentsline {section}{\numberline {7.1}Community detection in dense graphs}{366}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}The stochastic block model}{366}{subsection.7.1.1}%
\contentsline {subsubsection}{Erd\H {o}s-R\'enyi random graphs and the modularity matrix}{366}{section*.177}%
\contentsline {paragraph}{The adjacency matrix.}{366}{section*.178}%
\contentsline {paragraph}{The modularity matrix.}{367}{section*.179}%
\contentsline {subsubsection}{From Erd\H {o}s-R\'enyi to the SBM}{367}{section*.180}%
\contentsline {subsubsection}{Case study: $2$-class symmetric SBM}{372}{section*.181}%
\contentsline {subsection}{\numberline {7.1.2}The degree-corrected stochastic block model}{376}{subsection.7.1.2}%
\contentsline {section}{\numberline {7.2}From dense to sparse graphs: a different approach}{384}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}The non-backtracking matrix}{385}{subsection.7.2.1}%
\contentsline {subsection}{\numberline {7.2.2}The Bethe Hessian matrix}{386}{subsection.7.2.2}%
\contentsline {subsection}{\numberline {7.2.3}Degree regularization}{387}{subsection.7.2.3}%
\contentsline {subsection}{\numberline {7.2.4}A unifying approach adapted to DC-SBM}{387}{subsection.7.2.4}%
\contentsline {section}{\numberline {7.3}Concluding remarks}{391}{section.7.3}%
\contentsline {section}{\numberline {7.4}Practical course material}{392}{section.7.4}%
\contentsline {chapter}{\numberline {8}Universality and Real Data}{395}{chapter.8}%
\contentsline {section}{\numberline {8.1}From Gaussian mixtures to concentrated random vectors and GAN data}{395}{section.8.1}%
\contentsline {subsection}{\numberline {8.1.1}On data models in large dimensions}{395}{subsection.8.1.1}%
\contentsline {subsection}{\numberline {8.1.2}A study of GAN-generated data}{398}{subsection.8.1.2}%
\contentsline {subsubsection}{Reminders on deep neural networks and GANs}{398}{section*.191}%
\contentsline {subsubsection}{GAN-induced data are concentrated random vectors}{399}{section*.193}%
\contentsline {subsubsection}{From GAN-data to CNN-features to GMM}{400}{section*.194}%
\contentsline {subsubsection}{Kernel asymptotics and GAN-generated data}{402}{section*.195}%
\contentsline {subsubsection}{Beyond ``classical'' kernels}{402}{section*.198}%
\contentsline {section}{\numberline {8.2}Wide-sense universality in large dimensional machine learning}{405}{section.8.2}%
\contentsline {section}{\numberline {8.3}Discussions and conclusions}{408}{section.8.3}%
\contentsline {chapter}{\numberline {9}Index}{411}{chapter.9}%
