# RMT4ML book to-do list

[Status] Responsible: To-do

- [] add external refs and SoTA papers
- [] we should keep the tables!, figures! (color, caption position, etc) and notations of matrix, 0 and 1, as consistent as possible!
- [] a table of large n,p consistent estimators
- [] change Python code: avoid X@A@X.T or D@Q and use instead X.dot(A.dot(X.T)) or np.multiply(d,Q)



- [x] Romain: community detection exo: Arun results Gaussian fluctuation
- [x] Romain: concentration exo Cosme


- [x] Zhenyu: finish Section 6.4 "large dimensional SVM"
- [x] Zhenyu: read again Section 3.1.3 MUSIC
- [x] Zhenyu: add Figure for P50 (RMT4ML/extra_plots.m)
- [] Zhenyu: figures 4.13-4.15 to be done!
- [x] Zhenyu: Sec 4.5.5 Summary of Section 4.5: consistent estimator of data statistics, choice of kernel function
- [x] Zhenyu: 8.3 Discussions and conclusions
- [] Zhenyu: Matlab Data download and add to all real-world data scripts
- [] Zhenyu: go over Python codes of Romain and Matlab codes
- [] Zhenyu: Sec 6.2 SVM: performance compare? special case? add figure and simu
- [] Zhenyu: double check all practical course material, add simu 
- [] Zhenyu: check again all exo (Section 2.9), in particular exo 14 and 15.
- [] Zhenyu: adding website link to associated figures


## Code to-do list:

- [] find a way to automatically load EEG data (for both Matlab and Python code)
- [] if that also possible for kannada-MNIST?
- [] clean the Python code: e.g., use array instead np.mat that may appear at the several first scripts
- [] test all "switch cases"

Attention: 

* recentering of the k classes
	mean_selected_data  = np.mean(cascade_selected_data,axis=1).reshape(len(cascade_selected_data),1)
	norm2_selected_data = np.mean(np.sum(np.power(np.abs(cascade_selected_data-mean_selected_data),2),0))
	Il manquait le ",0" de la somme. Je n'avais pas remarqué le bug jusqu'alors mais ça change les amplitudes évidemment.
* comme par exemple des calculs de sqrtm(C) à l'intérieur d'une boucle où C ne bouge pas.


## About references 

Il faudra penser aussi à refaire un tour dans les références: 

- [] mettre à jour les réfs qui étaient en "arxiv" ou "submitted". Pour toutes, il faudrait au moins un lien. Si certaines ne donnent plus signe de vie, voir si on les élimine?
- [] rajouter des réfs récentes. En ce moment, je remarque que c'est la folie, il y a des papiers qui sortent de partout sur la RMT pour le ML. C'est vraiment le moment de sortir le livre!


Some feedbacks:

* figure should be placed on top of a page?
